
public class HelloWorld {
	public static void main(String[] args) {
		try {
			if ("Hey".equals(args[0])) {
		      		System.out.println("Hello World!");
	        	} else {
				System.out.println("Not today!");
			}
		} catch (ArrayIndexOutOfBoundsException e) {
		System.out.println("If you want me to say hello, you should do it first!");
		}
	}
}


